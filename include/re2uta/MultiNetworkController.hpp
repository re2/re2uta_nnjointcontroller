/*
 * MultiNetworkController.hpp
 *
 *  Created on: Jun 4, 2013
 *      Author: andrew.somerville
 */

#pragma once

#include <re2uta/TwoLayerNN.hpp>
#include <re2uta/NnController.hpp>
#include <re2uta/AtlasLookup.hpp>
#include <atlas_msgs/AtlasState.h>
#include <atlas_msgs/AtlasCommand.h>
#include <re2uta/AtlasLookup.hpp>
#include <re2/eigen/eigen_util.h>
#include <ros/ros.h>
#include <boost/tuple/tuple.hpp>
#include <boost/array.hpp>

namespace re2uta
{

class MultiNetworkController
{
    public:
        typedef boost::shared_ptr<MultiNetworkController> Ptr;

//        static const int numArmJoints  = 6;
//        static const int numLegJoints  = 6;
//        static const int numBackJoints = 3;

    public:
        MultiNetworkController( int numJoints, double posFilterGain, double velFilterGain );

        void addController(
                boost::tuple<int,int> const & range,
                double kappa,
                double kp,
                double kd,
                int    hiddenLayerSize,
                double firstLayerLearningRate,
                double lastLayerLearningRate );

        void setStateMsg( const atlas_msgs::AtlasStateConstPtr & stateMsg );

        void setCommandMsg( const atlas_msgs::AtlasCommandConstPtr & commandMsg );


        void updateParams( const atlas_msgs::AtlasStateConstPtr & stateMsg,
                           const atlas_msgs::AtlasCommandConstPtr & commandMsg,
                           double dt );

        Eigen::VectorXd calcTorques( double dt );

    protected:
        ros::NodeHandle      m_node;
        atlas_msgs::AtlasStateConstPtr   m_lastAtlasState;
        atlas_msgs::AtlasCommandConstPtr m_lastAtlasCommand;

//        re2uta::NnController m_lArmController;
//        re2uta::NnController m_rArmController;
//        re2uta::NnController m_lLegController;
//        re2uta::NnController m_rLegController;
//        re2uta::NnController m_backController;

        std::vector<NnController::Ptr> m_controllers;

        Eigen::VectorXd      m_desiredPositions;
        Eigen::VectorXd      m_positions;
        double               m_filterPosGain;
        Eigen::VectorXd      m_desiredVelocities;
        Eigen::VectorXd      m_velocities;
        double               m_filterVelGain;
        int                  m_numJoints;

        std::vector<boost::tuple<int,int> > m_controllerRanges;


    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

}
