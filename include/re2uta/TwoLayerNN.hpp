/*
 * TwoLayerNN.hpp
 *
 *  Created on: Apr 15, 2013
 *      Author: Ghassan Atmeh, Isura Ranatunga, andrew.somerville
 */

#pragma once

/**
 *
 * twoLayerNN: this is a class file that contains the two layer NN code
 *
 *
 * @author Isura Ranatunga, University of Texas at Arlington, Copyright (C) 2013.
 * @contact isura.ranatunga@mavs.uta.edu
 * @see ...
 * @created 02/13/2013
 * @modified 02/14/2013
 *
 */

#include <Eigen/StdVector>
#include <Eigen/Geometry>
#include <Eigen/Core>
#include <Eigen/Dense>

#include <iostream>
#include <math.h>

#include <boost/shared_ptr.hpp>

//#include <twoLayerNN.h>


namespace re2uta
{

class TwoLayerNN
{
    public:
        typedef boost::shared_ptr<TwoLayerNN> Ptr;

    public:

        TwoLayerNN( int inputDim,
                    int outputDim,
                    int hiddenLayerSize,
                    double kappa,
                    double kp,
                    double kd,
                    double firstLayerLearningRate, //gFactor,
                    double lastLayerLearningRate );  //fFactor );

        virtual ~TwoLayerNN();

        void setKp( double value );
        void setKd( double value );

        Eigen::VectorXd getFilteredError();
        Eigen::VectorXd getRobustifyingSignal();

        // This would give the W'sigma(V'x)
        Eigen::VectorXd
        NNAug_out( const Eigen::VectorXd& qDes,
                   const Eigen::VectorXd& q,
                   const Eigen::VectorXd& qDesDot,
                   const Eigen::VectorXd& qDot,
                   double dt);

    private:
        //This would give the update V(k+1) = V(k) + V'sigma(V'x)
        void VAug_update( const Eigen::VectorXd& q, const Eigen::VectorXd& r, const Eigen::MatrixXd & sigmaPrime, double delT);
        void WAug_update( const Eigen::VectorXd& q, const Eigen::VectorXd& r, const Eigen::MatrixXd & sigmaPrime, double delT);

    private:

        Eigen::MatrixXd V_trans;
        Eigen::MatrixXd W_trans;
        Eigen::MatrixXd G;
        Eigen::MatrixXd F;
        Eigen::MatrixXd L;
        Eigen::MatrixXd Z;
        Eigen::MatrixXd V_trans_next;
        Eigen::MatrixXd W_trans_next;

        Eigen::VectorXd x;
        Eigen::VectorXd y;
        Eigen::VectorXd hiddenLayer_out;
        Eigen::VectorXd hiddenLayer_in;
        Eigen::VectorXd outputLayer_out;
        Eigen::VectorXd    r;
        Eigen::VectorXd vRobust;
        int inputDim;
        int hiddlenLayerSize;
        int outputDim;
        double kappa;
        double kz;
        double zb; //delT

    public:
        double kp;
        double kd;
};

}

