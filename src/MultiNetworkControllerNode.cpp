/*
 * MultiNetworkControllerNode.cpp
 *
 *  Created on: May 28, 2013
 *      Author: andrew.somerville
 */


#include <re2uta/MultiNetworkController.hpp>
#include <atlas_msgs/SetJointDamping.h>

#include <re2uta/TwoLayerNN.hpp>
#include <re2uta/NnController.hpp>
#include <re2uta/AtlasLookup.hpp>
#include <atlas_msgs/AtlasState.h>
#include <atlas_msgs/AtlasCommand.h>
#include <re2uta/AtlasLookup.hpp>
#include <re2/kdltools/kdl_tree_util.hpp>
#include <re2/eigen/eigen_util.h>

#include <ros/ros.h>
#include <string>








class MultiNetworkControllerNode
{
    public:
        MultiNetworkControllerNode()
        {
            typedef atlas_msgs::AtlasState Atlas;

            boost::array<boost::tuple<int,int>,5> ranges;
            ranges[0] = boost::tuple<int,int>( Atlas::l_arm_shy, Atlas::l_arm_wrx ); // warning implicit order!
            ranges[1] = boost::tuple<int,int>( Atlas::r_arm_shy, Atlas::r_arm_wrx );
            ranges[2] = boost::tuple<int,int>( Atlas::l_leg_hpz, Atlas::l_leg_akx );
            ranges[3] = boost::tuple<int,int>( Atlas::r_leg_hpz, Atlas::r_leg_akx );
            ranges[4] = boost::tuple<int,int>( Atlas::back_bkz,  Atlas::back_bkx );

            m_controller.reset( new re2uta::MultiNetworkController( 28, 30, 30 ) );
            m_controller->addController( ranges[0], 0.09,  5, 10,  8, 25, 15 );
            m_controller->addController( ranges[1], 0.09,  5, 10,  8, 25, 15 );
            m_controller->addController( ranges[2], 0.10, 15, 15,  8, 25, 25 );
            m_controller->addController( ranges[3], 0.10, 15, 15,  8, 25, 25 );
            m_controller->addController( ranges[4], 0.30, 25, 25, 10, 25, 25 );

            m_controllerPeriod = ros::Duration( 0.002 );

            std::string urdfString;
            m_node.getParam( "/robot_description", urdfString );

            urdf::Model urdfModel;
            urdfModel.initString( urdfString );

            m_atlasLookup.reset( new re2uta::AtlasLookup( urdfModel ) );
            m_timerBool = false;

            m_atlasStateSub   = m_node.subscribe( "/atlas/atlas_state",  1, &MultiNetworkControllerNode::handleAtlasState,   this );
            m_atlasCommandSub = m_node.subscribe( "/walk/atlas_command", 1, &MultiNetworkControllerNode::handleAtlasCommand, this );
            m_atlasCommandPub = m_node.advertise<atlas_msgs::AtlasCommand>( "/atlas/atlas_command", 1 );
            m_commandTimer    = m_node.createTimer( m_controllerPeriod, &MultiNetworkControllerNode::sendCommand, this ); //, false, false);

            m_setDampingSrv   = m_node.serviceClient<atlas_msgs::SetJointDamping>( "/atlas/set_joint_damping" );

            m_setDampingSrv.waitForExistence();

            atlas_msgs::SetJointDamping transaction;
            transaction.request.damping_coefficients.fill( 80 );
            m_setDampingSrv.call( transaction );
        }

        void go()
        {
            ros::spin();
        }


    protected:

        void handleAtlasState( const atlas_msgs::AtlasStateConstPtr & msg )
        {
            if( !m_timerBool )
            {
                m_commandTimer.start();
                m_timerBool = true;
            }

            m_lastAtlasState = msg;
        }

        void handleAtlasCommand( const atlas_msgs::AtlasCommandConstPtr & msg )
        {
            m_lastAtlasCommand = msg;
        }

        void sendCommand( const ros::TimerEvent & event )
        {
            Eigen::VectorXd torques;

            m_controller->setStateMsg(   m_lastAtlasState   );
            m_controller->setCommandMsg( m_lastAtlasCommand );
            torques = m_controller->calcTorques( m_controllerPeriod.toSec() );

            if( torques.size() == 0 )
                return;

            atlas_msgs::AtlasCommand::Ptr atlasCommandMsg;

            atlasCommandMsg = m_atlasLookup->createEmptyJointCmdMsg();
            atlasCommandMsg->k_effort = std::vector<uint8_t>( m_atlasLookup->getNumCmdJoints(), 255 );
            atlasCommandMsg->effort   = re2::toStd( torques );
            atlasCommandMsg->desired_controller_period_ms = 5;

            m_atlasCommandPub.publish( atlasCommandMsg );
        }


    protected:
        ros::NodeHandle          m_node;
        ros::ServiceClient       m_setDampingSrv;
        re2uta::AtlasLookup::Ptr m_atlasLookup;
        ros::Duration            m_controllerPeriod;
        ros::Timer               m_commandTimer;
        ros::Subscriber          m_atlasStateSub;
        ros::Subscriber          m_atlasCommandSub;
        ros::Publisher           m_atlasCommandPub;
        atlas_msgs::AtlasStateConstPtr    m_lastAtlasState;
        atlas_msgs::AtlasCommandConstPtr  m_lastAtlasCommand;
        re2uta::MultiNetworkController::Ptr    m_controller;
        bool                     m_timerBool;

};



int main(int argc, char** argv)
{
    ros::init(argc, argv, "nn_multi_network_control_node");

    ros::NodeHandle node;

    std::string urdfString;
    node.getParam( "/robot_description", urdfString );


    // FIXME the important parameters are in hard copy with Gus. Need to collate them with this package
    MultiNetworkControllerNode multiNetworkControllerNode;

    multiNetworkControllerNode.go();

    return 0;
}
